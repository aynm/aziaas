## **Script Installation steps**
---

```ps
$User     = 'aynm'
$Repo     = 'AzIaaS'
$Script   = 'Hello-World' #Replace with Script name
$DestPath = [Environment]::GetFolderPath('MyDocuments')+"\PowerShell\Scripts"  																							 #User's default script folder

(New-Object System.Net.WebClient).DownloadFile("https://bitbucket.org/$User/$Repo/raw/f8775e9da8bdcc770e455f425a078546d76eb7dd/$Script.ps1","$DestPath\$Script.ps1")     #Download script. The GUID needs to be updated
If (($env:PATH -split ';') -notcontains $DestPath) {$env:Path += ";$DestPath"}     																						 #Add Script folder path to environment variable, if not present, for intellisense.

```
