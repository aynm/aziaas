#requires -Version 5.0 -Module Az.Monitor,Az.Accounts
<#PSScriptInfo
.VERSION 0.1
.GUID aaa68dab-9130-4c5d-83cc-f5f19b61b12f
.AUTHOR Ayan Mullick
.COMPANYNAME Ayan Mullick LLC
.COPYRIGHT
.TAGS AzCompute AzVM BitBucket Infra-Report
.LICENSEURI https://choosealicense.com/licenses/mit/
.PROJECTURI https://bitbucket.org/aynm/aziaas
.ICONURI
.EXTERNALMODULEDEPENDENCIES
.REQUIREDSCRIPTS
.EXTERNALSCRIPTDEPENDENCIES
.RELEASENOTES
#>

<#
.Synopsis
   This is a script to find the basic details of an Azure VM that's already been deleted.It iterates for each unique VM deletion activity and tries to locate the VM and NIC creation activity log if the VM was created within 90 days. 
   One needs read access to a subscription to run this script. One can IM ayan@mullick.in on Teams if one needs to add parameters to it.
.DESCRIPTION
   This is a script to find the basic details of an Azure VM that's already been deleted.
.EXAMPLE
   Login-AzAccount -Tenant <Tenantid> -Subscription <Subscriptionid>
   Get-AzVMDeletionActivity
.EXAMPLE
   Another example of how to use this cmdlet
#>

Begin {$Params = @{DefaultProfile  = $($Context=Get-AzContext;$Context); StartTime = $($Starttime=(Get-Date).AddDays(-90); $Starttime); ErrorAction='Stop' ; Verbose=$true }
       if ($Context) {$DeletionActivity=(Get-AzActivityLog @Params -ResourceProvider Microsoft.Compute).Where{$_.OperationName.Value -EQ 'Microsoft.Compute/virtualMachines/delete'}|Sort-Object ResourceId,CorrelationId -Unique  } 
      }
 
Process {$DeletionActivity.ForEach {$RId = $_.ResourceId
         $Creation=(Get-AzActivityLog @Params -ResourceId $RId).Where{($_.ResourceId -eq $RId -and $_.substatus.value -eq 'Created')}|Select-Object -Last 1
         if ($Creation) {$Responsebody=(New-Object PSObject -Property ([hashtable]$($Creation.Properties.Content))).responseBody|ConvertFrom-Json
                         $CreationProp=$Responsebody.properties}

         if ($CreationProp) {$NicCreation=(((Get-AzActivityLog @Params -EndTime $Creation.EventTimestamp -ResourceId $CreationProp.networkProfile.networkInterfaces.id).Where{$PSItem.Properties.Content.Keys -match 'responsebody'})[0])
                              if ( $NicCreation ) {$PIP=   ((New-Object PSObject -Property ([hashtable]$NicCreation.Properties.Content)).responsebody|ConvertFrom-Json).properties.ipConfigurations.properties.privateIPAddress}
                             }
    [PSCustomObject]@{
            AzVMname      = $RId.Split("/")[8]
            Hostname      = $CreationProp.osProfile.computerName
            AzVMId        = $CreationProp.VMId
            OSType        = $CreationProp.storageprofile.osdisk.ostype
            CreatedUTC    = $Creation.EventTimestamp
            CreatedBy     = $Creation.Caller
            IP            = $PIP
            Subscription  = ($Context.Name).Substring(0,$Context.Name.IndexOf('('))
            SubscriptionId= $_.SubscriptionId
            Location      = $Responsebody.location
            ResourceGroup = $_.resourceGroupName
            DeletedUTC    = $_.eventTimestamp
            DeletedBy     = $_.Caller
            Operation     = $_.operationName.value
                     }
                           }
        }

End {   }